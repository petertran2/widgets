import React, {Component} from 'react'

export default class Tab extends Component {
	constructor(props) {
		super(props)
		this.state = { tabIndex : 0 }
	}
	
	render() {
		return (
			<div>
				<h1>Tabs</h1>
				<div className='tabs'>
					<ul className='tab-header'>
						{this.props.tabs.map((tab, index) => {
							return (
								<li key={index}
									onClick={() => this.setState({ tabIndex : index })}>
									{tab.title}
								</li>
							)
						})}
					</ul>
				</div>
				<div className='tab-content'>
					<article>
						{this.props.tabs[this.state.tabIndex].content}
					</article>
				</div>
			</div>
		)
	}
}
