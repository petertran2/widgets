import React, {Component} from 'react'

export default class Weather extends Component {
	constructor(props) {
		super(props)
		this.state = { weather: null }
		this.success = this.success.bind(this)
	}
	
	componentDidMount() {
		navigator.geolocation.getCurrentPosition(this.success)
	}
	
	success(position) {
		let url = 'http://api.openweathermap.org/data/2.5/weather?'
		url += `lat=${position.coords.latitude}&lon=${position.coords.longitude}`
		url += '&APPID='
		url += '&units=imperial'
		
		let request = new XMLHttpRequest()
		request.onreadystatechange = () => {
			if (request.readyState === XMLHttpRequest.DONE) {
				if (request.status === 200) {
					this.setState({weather: JSON.parse(request.responseText)})
				}
			}
		}
		request.open('GET', url, true)
		request.send()
	}
	
	render() {
		const weather = this.state.weather
    return (
      <div>
        <h1>Weather</h1>
        <div className='weather'>
          <p>{weather ? weather.name : 'Loading...'}</p>
          <p>{weather ? `${weather.main.temp} F` : ''}</p>
        </div>
      </div>
    );
  }
}
