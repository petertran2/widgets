import React from 'react'
import ReactDOM from 'react-dom'
import Clock from './clock'
import Tabs from './tabs'
import Weather from './weather'

document.addEventListener("DOMContentLoaded", () => {
  ReactDOM.render(<Root />, document.querySelector('#main'))
})

const tabs = [
	{ title: 'one', content: 'First' },
	{ title: 'two', content: 'Second' },
	{ title: 'three', content: 'Third' }
]

function Root() {
  return (
    <div>
      <Clock />
      <Weather />
      <Tabs tabs={tabs} />
    </div>
  )
}
